# Project 5: Brevet time calculator with Ajax and MongoDB

Reimplement the RUSA ACP controle time calculator with flask ajax and MongoDB.

Simple list of controle times stored in a MongoDB database

## ACP controle times

That's "controle" with an 'e', because it's French, although "control" is also accepted. Controls are points where a rider must obtain proof of passage, and control[e] times are the minimum and maximum times by which the rider must arrive at the location.   

The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).

This project is  essentially replacing the calculator here (https://rusa.org/octime_acp.html). and storing the results in a MongoDB database

## AJAX and Flask reimplementation

The RUSA controle time calculator is a Perl script that takes an HTML form and emits a text page in the above link. 

* Each time a distance is filled in, the corresponding open and close times will be filled in with Ajax.   

* The logic for the calculations is in ./DockerMongo/acp_times.py

* webpage is in ./DockerMongo/templates/calc.html, and flask implementaion is in ./DockerMongo/flask_brevets.py

* system is run using docker-compose up. Docker files are ./DockerMongo/Dockerfile and ./DockerMongo/docker-compose.yml

