"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
import os
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
from pymongo import MongoClient

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'],27017)
db = client.mydb
###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    distance = request.args.get('distance', default=200, type=float)
    time = request.args.get('time', type=str)
    date = request.args.get('dat', type=str)
    arrow_time = arrow.get((date + ' ' + time + ':00'), 'YYYY-MM-DD HH:mm:ss')
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    open_time = acp_times.open_time(km, distance, arrow_time.isoformat())
    close_time = acp_times.close_time(km, distance, arrow_time.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/sub', methods=['POST'])
def sub():
    table = {'location' : request.form['location'],
             'km' : request.form['km'],
             'open' : request.form['open'],
             'close' : request.form['close'],
             }
    db.mydb.insert_one(table)

    return flask.redirect(flask.url_for('index'))

@app.route('/disp', methods=['POST'])
def display():
    _times = db.mydb.find()
    times = [time for time in _times]

    return flask.render_template('display.html', times=times)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
