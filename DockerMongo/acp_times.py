"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#

MAX_SPEEDS = [34, 32, 30, 28, 26]

MIN_SPEEDS = [15, 15, 15, 11.428, 13.333]

SPEED_CHANGES = [0, 200, 400, 600, 1000, 1300]

DIM = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

def calc_helper(con_dist):
    """
    determines how many additions will have to be made

    """
    if con_dist <= 60:
        return 1
    elif 60 <= con_dist <= 200:
        return 2
    elif 200 <= con_dist <= 400:
        return 3
    elif 400 <= con_dist <= 600:
        return 4
    elif 600 <= con_dist <= 1000:
        return 5
    elif 1000 <= con_dist <= 1300:
        return 6

    
def time_additions(h, m, brevtime):
    d = 0
    mchange = 0
    ychange = 0
    if brevtime.minute + m >=60:
        h += 1
        m = brevtime.minute + m - 60
    else:
        m = brevtime.minute + m
    d += h // 24
    h -= (24 * d)
    if brevtime.hour + h >= 24:
        d += 1
        h = brevtime.hour + h - 24
    else:
        h = brevtime.hour + h
    if d + brevtime.day > DIM[brevtime.month - 1]:
        mchange = 1
        d = brevtime.day + d - DIM[brevtime.month - 1]
    if (mchange + brevtime.month) > 12:
        ychange = 1
        mchange = 1
        d = brevtime.day + d - DIM[brevtime.month - 1]
    mchange=int(mchange)
    d = int(d)
    h = int(h)
    m = round(m)
    if ychange == 1:
        new_time = brevtime.replace(year=+(brevtime.year + 1), month=+mchange, day=+d, hour=+h, minute=+m)
    elif mchange >= 1:
        new_time = brevtime.replace(month=+brevtime.month+mchange,day=+d, hour=+h, minute=+m)
    elif d >= 1:
        new_time = brevtime.replace(day=+brevtime.day+d, hour=+h, minute=+m)
    else:
        new_time =  brevtime.replace(hour=+h, minute=+m)
    return new_time
    
    
def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km
    calculations = calc_helper(control_dist_km) - 1
    if calculations == 0:
        calculations = 1
    ret_time = 0
    for i in range(calculations - 1):
        ret_time += (SPEED_CHANGES[i+1] - SPEED_CHANGES[i]) / MAX_SPEEDS[i]
    ret_time += (control_dist_km - SPEED_CHANGES[calculations - 1]) / MAX_SPEEDS[calculations-1]
    hours = int(ret_time)
    mins = round((ret_time - hours) * 60)
    arrtime = arrow.get(brevet_start_time)
    new_time = time_additions(hours, mins, arrtime)
    return new_time.isoformat()

    
    
def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    if control_dist_km > brevet_dist_km:
        control_dist_km = brevet_dist_km
    calculations = calc_helper(control_dist_km)
    if calculations == 1:
        ret_time = (control_dist_km / 20) + 1
        hours = int(ret_time)
        minutes = (ret_time - hours) * 60
        arrtime = arrow.get(brevet_start_time)
        new_time = time_additions(hours, minutes, arrtime)
        return new_time.isoformat()
    calculations = calculations -1
    ret_time = 0
    for i in range(calculations - 1):
        ret_time += (SPEED_CHANGES[i+1] - SPEED_CHANGES[i]) / MIN_SPEEDS[i]
    ret_time += (control_dist_km - SPEED_CHANGES[calculations - 1]) / MIN_SPEEDS[calculations-1]
    hours = int(ret_time)
    mins = round((ret_time - hours) * 60)
    if control_dist_km >= brevet_dist_km:
        if brevet_dist_km == 200:
            mins += 10
        elif brevet_dist_km == 400:
            mins += 20
    arrtime = arrow.get(brevet_start_time)
    new_time = time_additions(hours, mins, arrtime)
    return new_time.isoformat()

